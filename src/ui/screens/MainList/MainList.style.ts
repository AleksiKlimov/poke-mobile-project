import ReactNative from 'react-native';

const style = ReactNative.StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
});

export default style;
