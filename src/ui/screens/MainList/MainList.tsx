import React from 'react';
import { View, Text } from 'react-native';

import style from './MainList.style';

const MainList: React.FC = () => {
  return (
    <View style={style.sectionContainer}>
      <Text>MainList</Text>
    </View>
  );
};

export default MainList;
