import React from 'react';
import { View, Text } from 'react-native';

import style from './UserAccount.style';

const UserAccount: React.FC = () => {
  return (
    <View style={style.sectionContainer}>
      <Text>UserAccount</Text>
    </View>
  );
};

export default React.memo(UserAccount);
