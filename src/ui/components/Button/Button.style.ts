import ReactNative from 'react-native';

const styles = ReactNative.StyleSheet.create({
  sectionContainer: {
    backgroundColor: 'skyblue',
    borderRadius: 16,
  },
  sectionBlock: {
    fontSize: 30,
    color: 'blue',
  },
});

export default styles;
