export type User = {
  email: string;
  userId: number;
  fullName?: string | null;
  avatar?: string | null;
};
