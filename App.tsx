/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import AppNavigation from './src/navigation/highStack/AppNavigation';

const App: React.FC = () => {
  return (
    <AppNavigation />
  );
};

export default App;
